from django.views.generic.list import ListView
from todos.models import TodoList


# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    template_name = "todo_lists/list.html"
